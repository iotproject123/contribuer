# Étapes à suivre pour apporter une contribution
* Créer un `issue` ou en trouver un. `gitlab`
* S'assigner l'`issue`. `gitlab`
* Créer une branche pour ses modifications. `gitlab`
* Se synchroniser avec la branche. `local`
* Apporter les modifications nécessaires pour résoudre l'`issue`. `locale`
* Pousser ses modifications sur la branche du serveur. `local`
* Créer un `merge request` pour `merge` sa branche avec la branche `master`. `gitlab`
* Ajouter un commentaire sur l'`issue` pour explique comment il a été réglé.
* Fermer l'issue. `gitlab`
* Supprimer la branche créée dans votre projet local et sur le serveur. `local`
Les étapes marquées `gitlab` doivent être faites dans l'interface de gitlab et celles marquée `local` doivent être faites dans votre terminal (sur Linux et Mac) ou votre git bash (sur Windows)

# Les `issues`
Dans le cadre de nos projets, les `issues` ne représenteront pas forcément des problèmes. Ils seront l'outil dont nous allons nous servir pour organiser notre travail. Toutes les modifications que nous allons apporter à un projet devront être reliée à un `issue`. Les `issues` devront aussi être reliés à des tags et, optionnellement, à des milestones. Voici une [vidéo](https://www.youtube.com/watch?v=tv4UM1ruQRs) intéressante sur la base des `issues` dans gitlab. Elle montre comment créer des `issues`, se les assigner, les fermer et créer des branches pour les régler.

# Les `merge request`
Les `merge request` sont des requêtes faites aux administrateurs pour intégrer les changements d'une branche à une autre. Ils permettent à l'administrateur et au dévelopeur d'avoir une discussion sur ce qui a été changé pour s'assurer que les branches peuvent être fusionnées sans qu'il y ait de conflits et de bugs. Vous pouvez visionner cette [vidéo](https://www.youtube.com/watch?v=Ddd3dbl4-2w) pour apprendre comment les créer et comment les gérer.
