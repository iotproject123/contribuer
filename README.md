# Comment contribuer aux différents projets
__Ceci est un guide destiné aux membres du groupe iotproject123 pour leur montrer comment contribuer aux différents projets du groupe.
Toute personne ne faisant n'allant pas au Cégep de l'Outaouais et n'étant pas membre du groupe iotproject123 ne peut contribuer aux projets de ce groupe.
Cependant, tous les projets du groupe sont sous la license MIT et peuvent donc être clonés et réutilisés en accord avec cette license.__

# Base de git
Avant de regarder le guide de contribution je vous encourage très fortement à regarder ce [document](https://gitlab.com/iotproject123/contribuer/blob/master/base_git.md) sur la base de git. Ce document explique comment créer un gérer des projets personnels sur git.

# Base de gitlab
Les vidéos 2, 4 et 6 de cette [série](https://www.youtube.com/watch?v=Jm9bcJCWg64&list=PLLnpHn493BHGgDmJGfCzRYRkFYWcRrxDT) expliquent très bien comment naviguer dans gitlab et l'utiliser.
Des références à ces vidéos se trouvent aussi dans le guide de contribution.

# Guide de contribution
Ce [guide](https://gitlab.com/iotproject123/contribuer/blob/master/guide_contribution.md) décris les étapes à suivre pour contribuer aux projet du groupe iotproject123. Il doit obligatoirement être suivi par tous les membres afin de s'assurer que les contributions aux projets soient simples à gérer pour les administrateurs.
