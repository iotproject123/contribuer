# Créer un projet
Il y a deux manières de créer un projet: le créer sur le serveur puis le cloner localement et le créer localement puis le pousser sur le serveur.
## Créer sur le serveur et le cloner localement
Pour créer un projet sur gitlab vous devez connecter à votre compte gitlab puis créer un projet sur le serveur. Vous devez par la suite [cloner le projet](#cloner-un-projet) pour y avoir accès localement.

## Créer localement et le pousser sur le serveur
```sh
# $PROJET
mkdir $PROJET
cd $PROJET
git init
git remote add origin https://gitlab.com/$USER/$PROJET.git
git commit -m "Initialisation"
git push origin master
```
Cette méthode va automatiquement initialiser un git `repository`. Il est a noté que l'accès du projet sera privé donc personne ne pourra voir le projet sauf vous. Pour changez cela, il faudra que vous alliez sur gitlab pour accéder à la configuration du projet en ligne. Il est aussi à noter que cette méthode ne fonctionne pas sur github.

# Cloner un projet
```sh
# $USER: Nom d'utilisateur ou du groupe possédant le projet.
# $PROJET: Le nom du projet.
git clone https://gitlab.com/$USER/$PROJET.git
```
L'action de cloner un projet le télécharge du serveur que nous puissions y accéder localement et y apporter des modifications. La commande `clone` va donc copier le git repository du url spécifié sur votre ordinateur dans un nouveau dossier de même nom que le projet. Ce dossier se situera dans le dossier dans lequel vous vous situez dans le git bash. Vous pouvez utiliser la commande `pwd` pour savoir où vous vous trouvez.

# Se synchroniser avec le serveur
```sh
# $BRANCHE: nom de la branche sur laquelle vous voulez vous synchroniser.
git pull origin $BRANCHE
# ou
git pull origin $(git rev-parse --abbrev-ref HEAD)
```
Se synchroniser nous permet de télécharger et d'appliquer localement tous les changements qui ont été faits sur le serveur et que nous ne possédons pas encore. Pour ce faire il faut spécifier le nom de la branche avec laquelle on veut se synchroniser. On va généralement utiliser la branche sur laquelle on se trouver localement, cependant git nous permet de nous synchroniser avec n'importe quelle branche donc il faut faire attention. Pour savoir sur quelle branche vous êtes vous pouvez utiliser la commande `git branch`. Cette commande imprime toutes les branches du projet et un `*` à côté de la branche sur laquelle vous vous trouvez.

# Créer des `commit`
```sh
# $FICHIERS: Fichiers modifiés
# $MESSAGE: Description des modifications apportées au projet
git add $FICHIERS
# ou
git add . # Ajouter les changements au groupe de changements
git commit -m "$MESSAGE" # Traiter les changements et leur donner une description
```
Les `commit` sont des groupes changements destinés à organiser les modifications qu'on apportent à un projet. Pour en créer, on doit commencer par ajouter les changemets au commit avec `git add` suivi du nom de chaque fichier qu'on veut ajouter ou bien de `.`. Le `.` est un raccourci qu'on utilise lorsque qu'on veut ajouter les changements qu'on a apportés à tous les fichiers. On peut par la suite utiliser `git commit` avec l'argument `-m` et un message. Le message doit être mis entre des guillemet et doit bien décrire les changements apportés de façon concise. Le message est très important puisqu'il sera vu par toutes les personnes ayant accès au projet.

# Envoyer ses changements sur le serveur
```sh
# $BRANCHE: Branche
git push origin $BRANCHE # Pousser le groupe de changemets sur le serveur
```
Pour pousser tous ses `commit` sur le serveur, on utilise la commande `git push` suivie de l'alias du serveur (généralement `origin`) et de la branche qu'on veut modifier. Il n'est pas nécessaire de pousser un commit avant d'en créer un autre localement puisqu'ils vont tous simplements s'accumuler et être tous envoyés sur le serveur en même temps lorsque la commande git push sera utilisée.

# Créer une nouvelle branche
```sh
# $BRANCHE: Nom de la branche que vous voulez créer.
git checkout -b $BRANCHE
```

# Passer d'une branche à l'autre
```sh
# $BRANCHE: Nom de la branche sur laquelle vous voulez aller.
git checkout $BRANCHE
```
Cette commande est assez claire, cependant rappelez-vous qu'avant de changer de branche vous devez avoir `commit` tous vos changements, mais vous n'avez pas besoin de les avoir poussez sur le serveur.

# Lister les branches du projet
```sh
git branch
```
La commande `git branch` va écrire une liste de toutes les branches du projet en mettant un `*` à côté de celle sur laquelle vous êtes.

# Fusionner des branches
```sh
# $BRANCHE_A: _______            Branche à fusionner
#                    \___
# $BRANCHE_B: ___________\_____  Branche fusionnée

git checkout $BRANCHE_A     # Aller sur la $BRANCHE_A
git add .
git commit -m "$MESSAGE"
git push origin $BRANCHE_A  # Pousser tous les changements de la $BRANCHE_A sur le serveur
git checkout $BRANCHE_B     # Aller sur la $BRANCHE_B
git pull origin $BRANCHE_A  # Fusionner les branches
git push origin $BRANCHE_B  # Pousser les changements sur la $BRANCHE_B
```

# Supprimer une branche
```sh
# $BRANCHE: Branche à supprimer
git branch -d $BRANCHE # Supprimer localement
git push -d origin $BRANCHE # Supprimer sur le serveur
```
Une fois qu'on a fusionné des branches l'une des branches devient souvent obsolète si on a plus besoin de s'en reservir. Lorsque vient le temps de supprimer une branche, il faut la supprimer localement et sur le serveur. Pour ce faire on utiliser l'argument `-d` (pour delete) avec le nom de la branche à supprimer. Après avoir supprimer la branche il faut passer à une branche qui existe avec la commande `git checkout`.
